package pitel.room.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface ConstantDao {
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insert(vararg constant: Constant): Array<Long>

	/*
	@Update(onConflict = OnConflictStrategy.REPLACE)
	fun update(vararg constant: Constant)

	@Delete
	fun delete(vararg constant: Constant)
	*/

	@Query("SELECT * FROM constant")
	fun getAll(): LiveData<List<Constant>>
}