package pitel.room.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Constant(@PrimaryKey val name: String, val value: Double)