package pitel.room.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = [(Constant::class)], version = 1)
abstract class ConstantDatabase : RoomDatabase() {
	abstract fun constantDao(): ConstantDao
}