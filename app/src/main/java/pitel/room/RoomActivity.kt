package pitel.room

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_room.*
import timber.log.Timber

class RoomActivity : AppCompatActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		val model = ViewModelProviders.of(this)[RoomViewModel::class.java]
		setContentView(R.layout.activity_room)

		list.setHasFixedSize(true)
		val adapter = ConstantAdapter()
		list.adapter = adapter

		Timber.d("Connecting to VM and getting data")
		model.constants.observe(this, Observer {
			Timber.d("Showing $it")
			adapter.submitList(it)
		})
		Timber.d("Connected to VM")

		if (savedInstanceState == null) {
			model.loadConstants()
		}
	}
}