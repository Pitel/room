package pitel.room

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.persistence.room.Room
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import pitel.room.db.Constant
import pitel.room.db.ConstantDatabase
import timber.log.Timber
import java.util.concurrent.TimeUnit
import kotlin.math.E
import kotlin.math.PI

class RoomViewModel(app: Application) : AndroidViewModel(app) {
	val db = Room.databaseBuilder(getApplication(), ConstantDatabase::class.java, "contacts").build()
	val constants = db.constantDao().getAll()

	fun loadConstants() {
		launch(CommonPool) {
			db.clearAllTables()
			delay(2, TimeUnit.SECONDS)
			with (db.constantDao()) {
				insert(Constant("Pi", PI))
				delay(1, TimeUnit.SECONDS)
				insert(Constant("e", E))
				delay(1, TimeUnit.SECONDS)
				insert(Constant("The Answer", 42.toDouble()))
			}
			Timber.d("DB filled")
		}
	}

	override fun onCleared() {
		db.close()
		super.onCleared()
	}
}