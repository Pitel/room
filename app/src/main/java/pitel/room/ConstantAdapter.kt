package pitel.room

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.extensions.LayoutContainer
import pitel.room.db.Constant

class ConstantAdapter : ListAdapter<Constant, ConstantAdapter.ViewHodler>(object : DiffUtil.ItemCallback<Constant>() {
	override fun areItemsTheSame(oldItem: Constant, newItem: Constant) = oldItem.name == newItem.name
	override fun areContentsTheSame(oldItem: Constant, newItem: Constant) = oldItem == newItem
}) {
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHodler(LayoutInflater.from(parent.context).inflate(android.R.layout.simple_list_item_2, parent, false))
	override fun onBindViewHolder(holder: ViewHodler, position: Int) = with(holder) {
		val constant = getItem(position)
		text1.text = constant.name
		text2.text = constant.value.toString()
	}

	inner class ViewHodler(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
		val text1: TextView = containerView.findViewById(android.R.id.text1)
		val text2: TextView = containerView.findViewById(android.R.id.text2)
	}
}